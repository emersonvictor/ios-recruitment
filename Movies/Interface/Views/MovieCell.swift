//
//  MovieCell.swift
//  Movies
//
//  Created by Emerson Victor on 12/11/19.
//  Copyright © 2019 Cubos. All rights reserved.
//

import UIKit
import SkeletonView

class MovieCell: UICollectionViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var poster: PosterImageView!
    @IBOutlet weak var title: UILabel!
    
    // MARK: - Initializer
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupAppearance()
        self.setupLoadState()
    }
    
    // MARK: - Setup appearance and load state
    func setupAppearance() {
        self.contentView.layer.cornerRadius = 7
        self.contentView.layer.masksToBounds = true
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.layer.shadowRadius = 7
        self.layer.shadowOpacity = 0.2
    }
    
    func setupLoadState() {
        self.poster.isSkeletonable = true
        self.title.isSkeletonable = true
        self.poster.showAnimatedGradientSkeleton()
        self.title.showAnimatedGradientSkeleton()
    }
}
