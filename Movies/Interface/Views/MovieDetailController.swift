//
//  MovieDetailController.swift
//  Movies
//
//  Created by Emerson Victor on 12/11/19.
//  Copyright © 2019 Cubos. All rights reserved.
//

import UIKit

class MovieDetailController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var moviePoster: PosterImageView!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var movieDescription: UITextView!
    
    // MARK: - Properties
    var movie: Movie?
    
    // MARK: - View cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let movie = self.movie {
            self.navigationItem.title = movie.title
            self.movieTitle.text = movie.title
            self.movieDescription.text = movie.description
            self.moviePoster.loadImageView(withUrl: movie.posterPath)
        }
    }
}
