//
//  MoviesController.swift
//  Movies
//
//  Created by Emerson Victor on 12/11/19.
//  Copyright © 2019 Cubos. All rights reserved.
//

import UIKit

class MoviesController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var moviesCollectionView: UICollectionView!
    
    // MARK: - Properties
    internal var movies: [Movie] = []
    internal var actualPage: Int = 0
    internal var totalPages: Int = 0
    internal let cellSpacing: CGFloat = 10
    
    // MARK: - View cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.moviesCollectionView.dataSource = self
        self.moviesCollectionView.delegate = self
        self.setupCollectionViewLayout()
        self.loadMoviesData()
    }
    
    // MARK: - Setup collection view flow layout
    func setupCollectionViewLayout() {
        let layout = UICollectionViewFlowLayout()
        
        layout.sectionInset = UIEdgeInsets(top: cellSpacing,
                                           left: cellSpacing,
                                           bottom: cellSpacing,
                                           right: cellSpacing)
        layout.minimumLineSpacing = cellSpacing
        layout.minimumInteritemSpacing = cellSpacing
        self.moviesCollectionView.collectionViewLayout = layout
    }
    
    // MARK: - Load movies data from API
    func loadMoviesData() {
        NetworkManager.isConnected { (isConnected) in
            if isConnected {
                self.requestMoviesFromApi()
            } else {
                self.showNetworkConnectionAlert()
            }
        }
    }
    
    func requestMoviesFromApi() {
        MoviesApiManager.getPopularMovies(fromPage: self.actualPage + 1) { (page, _, movies) in
            self.actualPage = page
            self.movies.append(contentsOf: movies)
            
            DispatchQueue.main.async {
                self.moviesCollectionView.reloadData()
            }
        }
    }
    
    func showNetworkConnectionAlert() {
        let alert = UIAlertController(title: "Error de conexão",
                                      message: "Ops, parece que você não tem internet, tente connectar novamente",
                                      preferredStyle: .alert)
        
        let tryAgainAction = UIAlertAction(title: "Tentar de novo",
                                           style: .default) { (_) in
            self.loadMoviesData()
        }
        
        let close = UIAlertAction(title: "Fechar", style: .destructive, handler: nil)
            
        alert.addAction(tryAgainAction)
        alert.addAction(close)
        self.present(alert,
                     animated: true,
                     completion: nil)
    }
    
    // MARK: - Send movie through segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMovieDetail" {
            let movieDetailController = segue.destination as! MovieDetailController
            movieDetailController.movie = sender as? Movie
        }
    }
}

// MARK: - UICollectionView DataSource
extension MoviesController: UICollectionViewDataSource {
    // Set collection view sections
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.movies.count == 0 ? 1 : self.movies.count
    }
    
    // Set cell content
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieCell", for: indexPath) as! MovieCell
        
        if self.movies.count > 0 {
            let movie = self.movies[indexPath.row]
            cell.title.hideSkeleton()
            cell.title.text = movie.title
            
            cell.poster.loadImageView(withUrl: movie.posterPath)
        }
        
        return cell
    }
}

// MARK: - UICollectionView delegates
extension MoviesController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    // Handle cell seletion
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        /// Check if has movies before go to detail controller
        if self.movies.count > 0 {
            let movie = self.movies[indexPath.row]
            self.performSegue(withIdentifier: "showMovieDetail", sender: movie)
        }
    }
    
    // Set cell size according to device width
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let deviceWidth = collectionView.bounds.width
        return CGSize(width: (deviceWidth/2)-cellSpacing*2, height: (deviceWidth/2)*1.5)
    }
    
    // Load cell at the end
    func collectionView(_ collectionView: UICollectionView,
                        willDisplay cell: UICollectionViewCell,
                        forItemAt indexPath: IndexPath) {
        
        if indexPath.row >= self.movies.count-1 {
            self.loadMoviesData()
        }
    }
}
