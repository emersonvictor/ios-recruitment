//
//  Movie.swift
//  Movies
//
//  Created by Emerson Victor on 12/11/19.
//  Copyright © 2019 Cubos. All rights reserved.
//

import UIKit

class Movie: Codable {
    let id: Int
    let title: String
    let description: String
    let posterPath: String

    enum CodingKeys: String, CodingKey {
        case id
        case title
        case description = "overview"
        case posterPath = "poster_path"
    }

    init(id: Int, title: String, description: String, posterPath: String) {
        self.id = id
        self.title = title
        self.description = description
        self.posterPath = posterPath
    }
}
