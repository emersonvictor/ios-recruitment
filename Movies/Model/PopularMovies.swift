//
//  PopularMovies.swift
//  Movies
//
//  Created by Emerson Victor on 12/11/19.
//  Copyright © 2019 Cubos. All rights reserved.
//

import Foundation

class PopularMovies: Decodable {
    let page, totalResults, totalPages: Int
    let movies: [Movie]

    enum CodingKeys: String, CodingKey {
        case page
        case totalResults = "total_results"
        case totalPages = "total_pages"
        case movies = "results"
    }

    init(page: Int, totalResults: Int, totalPages: Int, movies: [Movie]) {
        self.page = page
        self.totalResults = totalResults
        self.totalPages = totalPages
        self.movies = movies
    }
}
