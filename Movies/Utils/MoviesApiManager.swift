//
//  MoviesApiManager.swift
//  Movies
//
//  Created by Emerson Victor on 12/11/19.
//  Copyright © 2019 Cubos. All rights reserved.
//

import UIKit

class MoviesApiManager {
    
    // MARK: - Properties
    private static let apiKey = "ba993d6b1312f03c80a322c3e00fab4d"
    private static let baseApiUrl = "https://api.themoviedb.org/3/movie/popular"
    private static let baseImageApiUrl = "http://image.tmdb.org/t/p/"
    private static let language = "pt-BR"
    private static let imageSize = "w500"
    private static let imageCache = NSCache<NSString, UIImage>()
    
    // MARK: - Initializer
    private init() {}
    
    // MARK: - Api request
    class func getPopularMovies(fromPage page: Int = 1,
                                results: @escaping (_ page: Int, _ totalPages: Int, _ movies: [Movie]) -> Void) {
        
        let urlString = "\(baseApiUrl)?api_key=\(apiKey)&language=\(language)&page=\(page)"
        let url = URL(string: urlString)!
        
        URLSession.shared.dataTask(with: url) {(data, response, error) in
            /// Error case
            if let error = error {
                print("Error: \(error)")
                return
            }
            
            /// Empty case
            guard let _ = response, let data = data else {
                print("Empty response")
                return
            }
            
            /// Success case
            let popularMovies = try? JSONDecoder().decode(PopularMovies.self, from: data)
            if let popularMovies = popularMovies {
                results(popularMovies.page, popularMovies.totalPages, popularMovies.movies)
            }
        }.resume()
    }
    
    class func getMoviePoster(fromUrl imageUrl: String, result: @escaping (_ image: UIImage) -> Void) {
        if let image = self.imageCache.object(forKey: imageUrl as NSString) {
            result(image)
        } else {
            let urlString = "\(baseImageApiUrl)/\(imageSize)/\(imageUrl)?api_key=\(apiKey)&language=\(language)"
            let url = URL(string: urlString)!
            
            URLSession.shared.dataTask(with: url) {(data, response, error) in
                /// Error case
                if let error = error {
                    print("Error: \(error)")
                    return
                }
                
                /// Empty case
                guard let _ = response, let data = data else {
                    print("Empty response")
                    return
                }
                
                /// Success case
                if let image = UIImage(data: data) {
                    self.imageCache.setObject(image, forKey: imageUrl as NSString)
                    result(image)
                }
            }.resume()
        }
    }
}
