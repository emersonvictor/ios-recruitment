//
//  NetworkManager.swift
//  Movies
//
//  Created by Emerson Victor on 13/11/19.
//  Copyright © 2019 Cubos. All rights reserved.
//

import Foundation
import Network
import Connectivity

class NetworkManager {
    private static let connectivity = Connectivity()
    
    class func isConnected(status: @escaping (_ isConnected: Bool) -> Void) {
        self.connectivity.checkConnectivity { (connectivity) in
            switch connectivity.status {
            case .connected,
                 .connectedViaWiFi,
                 .connectedViaCellular:
                
                status(true)
                
            case .connectedViaWiFiWithoutInternet,
                 .connectedViaCellularWithoutInternet,
                 .notConnected,
                 .determining:
                
                status(false)
            }
        }
    }
}
