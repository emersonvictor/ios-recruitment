//
//  UIImageViewExtension.swift
//  Movies
//
//  Created by Emerson Victor on 12/11/19.
//  Copyright © 2019 Cubos. All rights reserved.
//

import UIKit
import SkeletonView

class PosterImageView: UIImageView {
    
    var posterPath: String?
    
    // MARK: - Load image from url
    func loadImageView(withUrl urlString: String) {
        self.posterPath = urlString
        self.image = nil
        
        MoviesApiManager.getMoviePoster(fromUrl: urlString) { (img) in
            DispatchQueue.main.async {
                if self.posterPath == urlString {
                    self.image = img
                    self.hideSkeleton()
                }
            }
        }
    }
}
